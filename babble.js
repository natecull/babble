/* 

	Babble 0.4
	A tiny Javascript to generate probabilistic random nonsense based on text files

	Much cheaper and faster than trying to train a neural network!

	written by Nate Cull 2019 - MIT license


	load with babble = require('./babble')

	run with babble.run(seed) or leave seed blank to get a random seed


	Set babble.charmode to true if you want to run in character rather than word mode


	Source files must be in the 'text' directory
	Output file will be in the current directory

	Rename the output file to something else if there was particularly interesting output

*/

const fs = require('fs')
const babble = {}
module.exports = babble


const rng = {

	// Park and Miller 1993 Minimal Standard random generator
	// Simplistic, but pretty good for generative art projects
	// https://en.wikipedia.org/wiki/Lehmer_random_number_generator
	// and http://www.firstpr.com.au/dsp/rand31/p1192-park.pdf 
	
	multiplier: 48271,
	modulus: 2147483647,
	seed: 1,


	// sets the seed to a predictable or random number

	reseed: function(s) {
		if (s == null) {
			s = Math.floor(Math.random() * rng.modulus)
		} 

		rng.seed = s
	},

	// roll a number between 0 and d-1

	roll: function(d) {

		rng.seed = rng.seed * rng.multiplier % rng.modulus

		return Math.floor(rng.seed % d)
	}
}

babble.rng = rng


babble.startmark = ' start'
babble.totalmark = ' total'
babble.memory = {}

babble.text = []
//babble.sources = ['starwars.txt','esb.txt','rotj.txt','tpm.txt','aotc.txt','rots.txt' ]
babble.sources = ['starwars.txt','esb.txt','rotj.txt','tpm.txt','aotc.txt','rots.txt','romeo.txt','julius.txt']

babble.outfile = 'babble.txt'
babble.samplechars = 300

babble.charmode = false

if (babble.charmode) {

	// character mode: 10000 characters, depth of 9 characters

	babble.amount = 10000
	babble.depth = 9

} else {

	// word mode: 2000 words, depth of 2 words

	babble.amount = 2000
	babble.depth = 2
	

}




/*

	parse a text file to an array of tokens 
	(either words or characters, depending on whether character mode is enabled)

	word '' indicates a linebreak

	The parser has a bit of logic in it because we need to compensate for common mangling of text files,
	such as adding line breaks and indentation that we need to ignore

*/

babble.parse = function(f,delim='\n') {
	
	console.log('Parsing ' + f)

	let text = fs.readFileSync('text/'+ f,'utf-8').split(delim)

	let a = []
	text.forEach(function(line) {

		// trim leading and trailing spaces from lines because we don't care about indentation

		let trimmed = line.trim()

		//console.log(trimmed)

		if (trimmed == '') { 

			// a blank line counts as the token '' whether it's a word or a character
			a.push('')

		} else if (babble.charmode) {

			// in character mode, each char becomes a token (ignoring Unicode astrals for now)
			// and add an extra space on the end of the line

			(trimmed + ' ').split('').forEach(char => a.push(char))
		} else {

			// in word mode, each word becomes a token, and collapse all whitespace to one space

			trimmed.split(' ').forEach(function(word) {
				let trimword = word.trim()
				if (trimword != '') {a.push(trimword)}
			})
		}
	})

	return a

}

// Parse a list of files

babble.parseall = function(... flist) {
	let a = []
	flist.forEach(function(f) {
		babble.parse(f).forEach(t => a.push(t) )
	})
	return a
}


// parse the whole text corpus

babble.loadtext = function() {
	babble.text= babble.parseall(... babble.sources)
}



// add one to the count at a given token's location in the memory

babble.inctoken = function(c, t) {
	if (c[t] == undefined) { c[t] = 0 }

	c[t]++
}

// Locate - creating if necessary - a state position in the memory

babble.locate = function(state) {
	let cursor = babble.memory
	let oldtoken = null
	let i = 0

	for (i=0;i<babble.depth;i++) {

		oldtoken = state[i]

		if (cursor[oldtoken] == undefined) {
			cursor[oldtoken] = {}
		}

		cursor = cursor[oldtoken]
	}

	// Initialize the total to null

	if (cursor[babble.totalmark] == undefined) { 
		cursor[babble.totalmark] = 0 
	}

	return cursor
}

// Return a fresh state array indicating the start of the document

babble.makestartstate = function() {
	let state = []
	let i = 0
	for (i=0;i<babble.depth;i++) { 
		state.push(babble.startmark) 
	}

	return state
	
}

// update a state to a new token (modifies the state object passed in)

babble.changestate = function(state,token) {

	state.shift()
	state.push(token)
}

// add a token to the memory given a state

babble.addtoken = function(state,token) {

	// locate our place in the memory based on the state

	let cursor = babble.locate(state)

	// add one to the total and the current token

	babble.inctoken(cursor,token)
	babble.inctoken(cursor,babble.totalmark)

	// update the state with the new token (modifies the state object passed in)

	babble.changestate(state,token)

}

// add a list of tokens in sequence to the memory, given a start state
// the state object will be modified

babble.addtokens = function(text,state) {
	text.forEach(function(token) {
		babble.addtoken(state,token)
	})
}

// get the total token count for a state

babble.gettotal = function(state) {
	return babble.locate(state)[babble.totalmark]
}

// get a list of tokens and their occurrence count (ie, probability, when divided by total count)

babble.gettokenlist = function(state) {
	let cursor = babble.locate(state)
	let keys = Object.keys(cursor).filter(t => t != babble.totalmark)

	return keys.map(key => [key,cursor[key]])
}

// given a state, guess a next token based on the counts and total in the memory
// Get a table of tokens and counts, and the total
// Get a random number from 1 to total
// Loop through the table subtracting the count from the random number until we hit zero

babble.guesstoken = function(state) {

	let range = babble.gettotal(state)
	//let r = Math.floor(Math.random() * range) +1
	let r = babble.rng.roll(range) + 1
	let table = babble.gettokenlist(state)

	let line = 0
	let token = null
	let prob = null


	while (r > 0) {
	
		token = table[line][0]
		prob = table[line][1]
		
		r = r - prob
		line++

	}

	return token

}

// Generate a sequence of tokens given a state

babble.gen = function(state,num) {

	let a = []
	let i = 0

	let token = null

	for (i=0;i<num;i++) {
		
		token = babble.guesstoken(state)

		a.push(token)

		babble.changestate(state,token)

	}

	return a
}

// Format a sequence of tokens into a string, either as words or as characters

babble.say = function(a)  {
	let s = ''

	a.forEach(function(t) {

		if (t == '') { 
			s += '\n\n' 
		} else if (babble.charmode) {
			{ s+= t }
		} else {
			{ s+= t + ' ' }
		}
	
	})

	return s
}

// interactively display a snippet of text at a given state with a random seed

babble.snippet = function(state,words=100,seed) {

	babble.rng.reseed(seed)
	console.log(babble.say(babble.gen(state,words)))
}

// generate and write, displaying a short sample of the output text

babble.write = function(f,words=1000) {

	let token = 'word'	

	if (babble.charmode) { token = 'character' }

	let output =  'Babble.js: ' + babble.amount + ' ' + token + 's of nonsense, depth ' + babble.depth + ', seed ' + babble.rng.seed + ' from ' + babble.sources.join(',') + '\n\n'
	
	output += babble.say(babble.gen(babble.makestartstate(),words))

	console.log("Sample of first " + babble.samplechars + " characters:\n")

	console.log(output.slice(0,300))

	fs.writeFileSync(f,output)
}


// make it go: parse, count and generate from the text corpus

// pass the random seed as a parameter if you want to repeat a text


babble.run = function(s) {

	babble.rng.reseed(s)

	console.log('Random seed is ', babble.rng.seed)
		
	let token = 'word'	

	if (babble.charmode) { token = 'character' }

	console.log('Parsing text corpus in ' + token + ' mode')

	babble.loadtext()

	console.log('' + babble.text.length + ' ' + token + 's parsed')

	console.log('Counting ' + token + ' frequencies')

	babble.memory = {}

	babble.addtokens(babble.text, babble.makestartstate())

	console.log('Generating to ' + babble.outfile)

	babble.write(babble.outfile,babble.amount)
}




