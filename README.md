# babble

version 0.4

By Nate Cull

Cheerful randomised nonsense without a neural network

This is very ancient technology (Markov chains), from the 1980s. It runs in seconds and generates randomised predictive text based on probabilities of successive tokens (words or characters) found in the source text.

>ACT I PROLOGUE 

>Enter DECIUS BRUTUS 

>Didst thou not a Jedi homing beacon? 

>CAPTAIN TYPHO Be safe, m'lady. 

>PADMÉ ...and the soft pink planet of the world. In truth, fair Montague, I am sure, I have walk'd about the ship. The ship is on a fallen log. 

>LEIA (angry) I am your father. 




Babble is a very simple Javascript Markov chain nonsense generator for text.

Make sure you're in the current directory

`node`

`babble = require('./babble')`

`babble.run()`

and it will parse the source files, generate text, and output it to the output file.

The source and output files are defined as the `babble.sources` and `babble.outfile` fields

It can operate in word or character mode. Word mode maybe generates more obviously English text, and has a much more recognisable dictionary, but character mode can give you a bit more creativity. Switch between modes by setting the `babble.charmode` field.

Then try fiddling with the `babble.depth`. The bigger the depth, the more it tends to repeat large set phrases, but the smaller, the less context anything has. Around 2 or 3 is usually good for words.

As of version 0.3, the random number generator uses the Park and Miller 1988 Minimal Standard algorithm as a tiny seedable generator. The important part of this is that you can now repeat text if you find a particularly interesting one. The generated text will show its random seed at the top; to run with a particular seed, use `babble.run(seed)` If you don't provide a seed number, a randomized one will be provided.

You can generate text of any length and seed from the state position by running `babble.snippet(state,length,seed)` where state is a list of words or characters.

The object `babble.memory` contains the lookup tables; it's most interesting in word mode. You could query it with, eg, `babble.memory.star` to see what the next likely words are.

>EXT. SPACE AROUND THE DEATH STAR 

>Luke's ship can be seen gliding across the dark side of the post. Within a moment, all the BATTLE DROIDS. PADME, OBI-WAN, and PALPATINE follow OBI-WAN along the wall of the empty cloak. As the door of the dark recesses only a thin wire from his cockpit. 

>They race for a moment the bolts of energy, evil lightning, shoot from the cave grows ever smaller. Han pulls back on the completed section like the star map hologram. 

>BIBBLE : Please, Your Majesty, there is no cave. 

>HAN No, I'm thinking a lot better having you here. I've been waiting for you, Count, this is. 

>QUI-GON: (V.O.) You will go to light-speed! 



There may well be many bugs. If it crashes, reload and try again.


>LUKE He ship and the heads from the fire ture. Evaculated, race explot time, is up and to the conces her comling case. 

>Artoo chank and ANA: Tolpation or seems. Where! 

>The weath gunprose throwth blashelt beling ships. 

>CRED - SER. There of the Owent so you. You've blassembracest done? Tomore collindow. SPACE 

